import java.util.concurrent.Semaphore;

public class Supermercado {
    public static void main(String[] args) {
        Semaphore semaforo = new Semaphore(2);
        Caja caja1 = new Caja("Caja 1");
        Caja caja2 = new Caja("Caja 2");
        Caja caja3 = new Caja("Caja 3");

        CajaHilo cajaHilo1 = new CajaHilo(caja1, semaforo, "king", 1);
        CajaHilo cajaHilo2 = new CajaHilo(caja2, semaforo, "redo", 2);
        CajaHilo cajaHilo3 = new CajaHilo(caja3, semaforo, "adri", 5);
    }
}
