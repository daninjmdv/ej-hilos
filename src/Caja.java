import java.util.concurrent.Semaphore;

public class Caja {
    public String nombre;

    public Caja(String nombre) {
        this.nombre = nombre;
    }

    public void procesarCompra(String cliente, int productos, Semaphore semaforo) {
        try {
            semaforo.acquire();
            System.out.println("La caja " + nombre + " recibe al cliente " + cliente);
            for (int i = 1; i <= productos; i++) {
                // procesar cada producto
                System.out.println("Caja " + nombre + " - Cliente: " + cliente + " - Producto " + i);
                // tiempo aleatorio entre 0 y 10 segundos
                int random = (int) (Math.random() * 10000);
                Thread.sleep(random);
            }
            System.out.println(nombre + " libre");
            semaforo.release();

        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }
}
