import java.util.concurrent.Semaphore;

public class ATM extends Thread{
    public String nombre;
    public Semaphore semaforo;


    public ATM(String nombre, Semaphore semaforo) {
        this.nombre = nombre;
        this.semaforo = semaforo;
    }
    @Override
    public void run() {

        int tiempo_uso = (int) (Math.random() * 8 + 3); // Genera un número aleatorio entre 3 y 10 segundos
        try {
            // Adquirir permiso del semáforo
            semaforo.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println(nombre + " se está utilizando");

        for (int i = 1; i <= tiempo_uso; i++) {
            try {
                sleep(1000); // Espera 1 segundo
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(nombre + " se está utilizando");
        }
        semaforo.release(); // Liberar permiso del semáforo
        System.out.println(nombre + " libre");

    }

}
